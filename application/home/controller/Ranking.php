<?php

// +----------------------------------------------------------------------
// | KyxsCMS Renew Novel Extend
// +----------------------------------------------------------------------
// | Copyright (c) Tealun Du All rights reserved.
// +----------------------------------------------------------------------
// | Licensed ( http://www.apache.org/licenses/LICENSE-2.0 )
// +----------------------------------------------------------------------
// | Author: Tealun Du
// +----------------------------------------------------------------------

namespace app\home\controller;

use app\common\controller\Base;
use app\common\model\Rank;


class Ranking extends Base
{
    /**
     * 自动更新榜单
     * 需配合服务器自动任务进行全量更新，或手动触发全量更新、指定范围（数据页码）更新
     * 服务器自动更新请在[计划任务]中设置[任务类型]为[访问URL](*此为宝塔设置方法，其他控制面板请咨询服务器提供商，暂未提供其他指引，后期会完善手动触发功能)
     * URL地址 http://www.yourwebsite.com/Home/Ranking/index
     */
    public static function index(){

        $dir = './runtime/log/ranking';
        if(!is_dir($dir)) mkdir($dir, 0755);

        //检测任务锁定
        $lockFile = $dir.'/processing.lock';
        if(is_file($lockFile)) exit('正在执行榜单刷新，请稍后再试');

        /** @var string $string 开始记录日志*/
        $time = time();
        $string =  date('Y-m-d H:i:s',$time).'开始执行榜单刷新任务'.PHP_EOL;

        $Rank = new Rank();
        if($list = $Rank->renewRank()){
            $list = json_decode($list,true);
        }else{
            exit('没有榜单可刷新');
        }

        file_put_contents($lockFile,serialize([
            'running'=>true
        ]));
        
        $count = count($list);
        $string .= '----------------------------------------'.PHP_EOL;
        $string .= '系统刷新'.$count.'条榜单，其中：'.PHP_EOL;

        foreach ($list as $k => $v){
            $string .= '----------------------------------------'.PHP_EOL;
            $string .= 'ID为'.$v['id'].'榜单"'.$v['name'].'"刷新数据：'.PHP_EOL;
            $books = json_decode($v['content']);
            foreach ($books as $key=>$book){
                $string .= ($key+1).'. "'.$book.'"'.PHP_EOL;
            }
        }
        $string .= '----------------------------------------'.PHP_EOL;

        $file = $dir.'/record'.$time.'.txt';
        file_put_contents($file,$string); //写入日志头部

        file_put_contents($file,'----------------------------------------'.PHP_EOL,FILE_APPEND);//完成日志
        file_put_contents($file,'本次榜单刷新任务完成。'.PHP_EOL,FILE_APPEND);
        file_put_contents($file,'----------------------------------------'.PHP_EOL,FILE_APPEND);//完成日志

        /** 完成后写入更新成功数据 供前端查阅 */
        $finishTime = time();
        $dataFile = $dir.'/renew_data'.$time.'.php';
        $total_time = $finishTime - $time;
        $total_time = self::secondToWord($total_time);
        $data = [
            'total_rank'=>$count,
            'renew_time'=>$time,
            'finish_time'=>$finishTime,
            'total_time'=>$total_time,

        ];
        file_put_contents($dataFile,serialize($data));

        //解除任务锁定
        unlink($lockFile);

        /** 释放变量 */
        unset($ids,$access,$dataFile,$file,$data,$list,$count,$time,$finishTime,$dir,$lockFile);
        exit();
    }

    public static function secondToWord($s=0){
        //计算分钟
        //算法：将秒数除以60，然后下舍入，既得到分钟数
        $m    =    floor($s/60);
        //计算秒
        //算法：取得秒%60的余数，既得到秒数
        $s    =    $s%60;
        //计算小时
        //算法一样
        $h=0;
        if($m >60){
            $h    =    floor($m/60);
            $m = $h%60;
        }
        $d=0;
        if($h>24){
            $d = floor($h/24);
            $h = $d%24;
        }
        //如果只有一位数，前面增加一个0
        $s = (strlen($s)==1)?'0'.$s:$s;
        $s = $s?$s.'秒':'';
        $m = $m?$m.'分':'';
        $d = $d?$d.'天':'';
        $h = $h?$h.'小时':'';
        return $d.$h.$m.$s;
    }
}
