﻿-- 纠正拼接到原生系统字符串后，分割SQL语句错误;
-- 扩展目录及配置
INSERT INTO `ky_menu` VALUES (1000,'小说更新工具',5,15,'admin/renew/index',0,'','工具',NULL);
INSERT INTO `ky_menu` VALUES (1001,'榜单更新工具',5,16,'admin/ranking/index',0,'','工具',NULL);

INSERT INTO `ky_config` VALUES (1000,'auto_renew_key',0,'自动更新密钥',8,'','自动更新需要的密钥，防止直接访问自动执行地址触发自动更新任务，无此项无法执行更新，为保证安全，请设置32位随机MD5加密字符串','',6,1);
INSERT INTO `ky_config` VALUES (1001,'string_filter_type',4,'敏感词过滤',2,'0:不过滤\r\n1:文字替换为*\r\n2:文字间间插入随机符号\r\n3:汉字转为拼音','小说文章敏感词模式，选择你想使用的模式。',0,6,1);
UPDATE `ky_config` SET `value` = '1:基本\r\n2:内容\r\n3:用户\r\n4:备份\r\n5:附件\r\n6:榜单' WHERE `id` = 13 LIMIT 1;
INSERT INTO `ky_config` VALUES (1002,'rank_site',4,'启用榜单',6,'0:不启用\r\n1:起点中文网\r\n2:纵横中文网','选择启用的榜单来源',0,6,1);
INSERT INTO `ky_config` VALUES (1003,'rank_replace_type',4,'榜单替换本地推荐模式',6,'0:不设置\r\n1:累加\r\n2:替换','榜单书籍替换本地推荐书籍的模式。不设置即保持现有手动推荐;累加即在现有推荐上增加榜单书籍推荐；替换即为清空本地推荐并将榜单书籍设为推荐。请选择你想使用的模式。',0,6,1);
INSERT INTO `ky_config` VALUES (1004,'p_start_type',4,'段首是否空两格',2,'0:否\r\n1:是','小说文章阅读体验模式，选择是否段首空两格。',0,6,1);
INSERT INTO `ky_config` VALUES (1005,'auto_renew_type',4,'自动更新模式',8,'0:全量范围\r\n1:指定页码范围\r\n2:指定小说ID范围','选择您希望的自动更新范围。',0,6,1);
INSERT INTO `ky_config` VALUES (1006,'auto_renew_start',0,'自动更新开始设置',8,'','自动更新需要的开始页数或开始小说ID，默认0',0,6,1);
INSERT INTO `ky_config` VALUES (1007,'auto_renew_end',0,'自动更新结束设置',8,'','自动更新需要的结束页数或结束小说ID，默认0',0,6,1);

-- 扩展榜单数据表
CREATE TABLE IF NOT EXISTS `ky_rank` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL DEFAULT '' COMMENT '榜单名称',
  `site` varchar(255) NOT NULL DEFAULT '' COMMENT '所属站点',
  `url` varchar(255) NOT NULL DEFAULT '' COMMENT '榜单获取地址',
  `cycle` tinyint(1) NOT NULL DEFAULT '0' COMMENT '更新周期 0:不更新 1:每天 2:每周 3：每月',
  `type` tinyint(1) NOT NULL DEFAULT '1' COMMENT '类型（起点中有，暂不明确是什么，先占位默认1）',
  `page_count` tinyint(1) NOT NULL DEFAULT '5' COMMENT '要获取的页数（起点为5页20条每页共获取100条）',
  `pattern` varchar(255) NOT NULL DEFAULT '' COMMENT '匹配正则（一般同一站点正则一样，不排除有不一样的）',
  `content` text NOT NULL COMMENT '榜单数据(json格式)',
  `explain` text NOT NULL COMMENT '说明',
  `add_time` int(11) NOT NULL DEFAULT '0' COMMENT '添加时间',
  `update_time` int(11) NOT NULL DEFAULT '0' COMMENT '上一次更新时间',
  `status` tinyint(1) NOT NULL DEFAULT '1' COMMENT '状态.1=有效,0=禁用',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 COMMENT='设置榜单表' AUTO_INCREMENT=1 ;
INSERT INTO `ky_rank` VALUES (1,'month_hot','qidian','https://www.qidian.com/rank/yuepiao',3,1,5,'/<div class="book-mid-info">.*?<h4><a.*?>(.*?)<\\/a><\\/h4>/','','起点中文网月票榜，每月更新',0,0,1);
INSERT INTO `ky_rank` VALUES (2,'week_hot','qidian','https://www.qidian.com/rank/fengyun',2,1,5,'/<div class="book-mid-info">.*?<h4><a.*?>(.*?)<\\/a><\\/h4>/','','起点中文网风云榜，每周更新',0,0,1);
INSERT INTO `ky_rank` VALUES (3,'day_hot','qidian','https://www.qidian.com/rank/hotsales',1,1,5,'/<div class="book-mid-info">.*?<h4><a.*?>(.*?)<\\/a><\\/h4>/','','起点中文网24热榜，每日更新',0,0,1);
INSERT INTO `ky_rank` VALUES (4,'month_hot','zongheng','http://www.zongheng.com/rank/details.html?rt=1&d=1&i=2',3,1,5,'/<div class="rank_d_b_name" title="(.*)">/','','纵横中文网月票榜，每月更新',0,0,1);
INSERT INTO `ky_rank` VALUES (5,'recommend','zongheng','http://www.zongheng.com/rank/details.html?rt=6&d=1',2,1,5,'/<div class="rank_d_b_name" title="(.*)">/','','纵横中文网推荐榜，每周更新',0,0,1);
INSERT INTO `ky_rank` VALUES (6,'day_hot','zongheng','http://www.zongheng.com/rank/details.html?rt=3&d=1',1,1,5,'/<div class="rank_d_b_name" title="(.*)">/','','纵横中文网24热榜，每日更新',0,0,1);
